package com.myzee.List;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class RemoveEleFromList {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<String> list = new ArrayList<>();
		list.add("Bob");
		list.add("Charlie");
		list.add("Katty");
		list.add("Alice");

		removeElement(list);
		System.out.println("\nafter removal\n");
		Iterator<String> i = list.iterator();
		// list.add("Jaclave"); // list cannot be modified once the iterator is created
		// for the same.
		// throws ConcurrentModificationException in i.next() method
		while (i.hasNext()) {
			System.out.println(i.next());
		}
		list.add("McGlen"); // list can be modified here, coz iterator has reached the end of the list by
							// traversing.
	}

	private static <T> void removeElement(List<T> list) {
//		 System.out.println("\nUsing Normal List");
//		 Iterator<T> itr = list.iterator();
//		 while(itr.hasNext()) {
//		 T next = itr.next();
//		 System.out.println(next);
//		 if (next.equals("Alice") || next.equals("Bob")) {
//		 itr.remove();
//		 }
//		 }

		System.out.println("\nUsing CopyOnWriteArrayList");
		List<T> l = new CopyOnWriteArrayList<>(list);
		Iterator<T> it = l.iterator();
		while (it.hasNext()) {
			T next = it.next();
			System.out.println(next);
			if (next.equals("Alice") || next.equals("Bob")) {
				l.remove(next);
			}
		}

		System.out.println("-------");
		Iterator<T> ii = l.iterator();
		while (ii.hasNext()) {
			T t = (T) ii.next();
			System.out.println(t);
		}
	}
}
