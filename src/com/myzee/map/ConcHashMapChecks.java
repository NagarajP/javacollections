package com.myzee.map;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ConcHashMapChecks {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Map<Integer, Integer> map = new ConcurrentHashMap<Integer, Integer>(15);
		map.put(1, 11);
		map.put(2, 22);
		map.put(3, 33);
		map.put(4, 44);
		// null cannot be added as key or value in ConcurrenctHashMap, it throws NullPointerException.
		map.put(null, 55);	//java.lang.NullPointerException
		map.put(5, null);	//java.lang.NullPointerException
		map.put(null, null);	//java.lang.NullPointerException
		
		Iterator<Integer> itr =  map.keySet().iterator();
		while (itr.hasNext()) {
			Integer i = (Integer) itr.next();
			System.out.println(map.remove(3));
			if(i == 2) {
				map.remove(2);
			}
		}
		System.out.println("============");
		map.forEach((k, v) -> System.out.println(k + " - " + v));
		System.out.println("============");
		
		Map<Integer, String> m = new HashMap<>();
		m.put(new Integer(1), "val1");
		m.put(new Integer(2), "val2");
		m.put(new Integer(3), "val3");
		m.put(new Integer(4), "val4");
		m.put(null, null);
		
		Iterator<Integer> it = m.keySet().iterator();
		while (it.hasNext()) {
			Integer j = (Integer) it.next();
//			System.out.println(j);
//			m.remove(2);
//			if(j == 3) {
//				it.remove();
//			}
		}
		System.out.println("===========");

		m.forEach((k, v) -> System.out.println(k + " -- " + v));
		
	}
}
